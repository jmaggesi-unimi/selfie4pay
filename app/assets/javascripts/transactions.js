  $(document).on('turbolinks:load', function() {
    var accepted, createDataTable, rejected, toapprove, all, sales, purchases, allTransactions;
    
    transactionColumns = [
          {
            data: 'id', searchable: 'false'
          }, {
            data: 'seller', searchable: 'true'
          }, {
            data: 'buyer', searchable: 'true'
          }, {
            data: 'transactiontime', searchable: 'false'
          }, {
            data: 'amount', searchable: 'false'
          }
        ]; 

    usersColumns = [
          {
            data: 'id', searchable: 'false'
          }, {
            data: 'email', searchable: 'true'
          }, {
            data: 'name', searchable: 'true'
          }, {
            data: 'surname', searchable: 'true'
          }
        ];

    clickOnRow = function(selector, table, tableType){
      $(selector).on( 'click', 'tr', function () {
        var id = table.api().row( this ).id();
        //console.log(id);
        if (tableType === 'transactions'){
          window.location = Routes.transaction_show_path(id);
          //console.log(window.location);
        } else {
          window.location = Routes.user_show_path(id);
          //console.log(window.location);
        }
        
      });
    };

    createDataTable = function(selector, columns, tableType) {
       var table = $(selector).dataTable({
        processing: true,
        serverSide: true,
        ajax: $(selector).data('source'),
        pagingType: 'full_numbers',
        columns: columns,
        searching: true
      });

      clickOnRow(selector, table, tableType);
      return table;
    };


    toapprove = createDataTable('#toapprove', transactionColumns, 'transactions');
    rejected = createDataTable('#rejected-table', transactionColumns, 'transactions');
    accepted = createDataTable('#accepted-table', transactionColumns, 'transactions');
    incomplete = createDataTable('#incomplete-table', transactionColumns, 'transactions');
    allTransactions = createDataTable('#allTransaction', transactionColumns, 'transactions');
    
    sales = createDataTable('#sells-table', transactionColumns, 'transactions');
    purchases = createDataTable('#buyers-table', transactionColumns, 'transactions');
    all = createDataTable('#all-users', transactionColumns, 'transactions');

    users = createDataTable('#users-table', usersColumns, 'users');

    $('.image-test').magnificPopup({
      type:'image',
      image: {
        verticalFit: true,
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function(item) {
          return item.el.attr('title');
        }
      },
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile mfp-with-zoom',
      closeOnContentClick: true,
      closeBtnInside: false,
      fixedContentPos: "auto",
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1]
      },
      zoom: {
        enabled: true,
        duration: 300
      }
    });

    $('#nav-admin a[data-toggle="tab"]').on('shown.bs.tab', function(e){      
      var filter = $(e.target).data('val');

      if (filter === 'toapprove')
        return toapprove.api().ajax.reload();

      if (filter === 'rejected')
        return rejected.api().ajax.reload();
      
      if (filter === 'accepted')
        return accepted.api().ajax.reload() ;

      if (filter === 'incomplete')
        return incomplete.api().ajax.reload() ;

      if (filter === 'all')
        return all.api().ajax.reload() ;

      if (filter === 'sells')
        return sales.api().ajax.reload() ;

      if (filter === 'buys')
        return purchases.api().ajax.reload() ;

      if (filter === 'allTransactions')
        return allTransactions.api().ajax.reload() ;

    });

    var customExport = $('#customExport').dataTable({
        processing: false,
        serverSide: false,
        pagingType: 'full_numbers',
        searching: true
    });

    createAutocomplete = function(selector, div){
      $(selector).autocomplete({
        source: "/user/search",
        minLength: 2,
        appendTo: div,
        select: function(e, ui){
          $(selector).val(ui.item.email);
          return false;
        }
      }).autocomplete("instance")._renderItem = function( ul, item ) {
        markup = ['<span class="title">' + item.full_name + '</span>',
        '<span class="author">' + item.email + '</span>']
        //"<div>" + item.full_name + "<br>" + item.email + "</div>"
        return $( "<li>" )
          .append( markup.join('') )
          .appendTo( ul );
      };
    }

    createAutocomplete(".seller-email", "#seller-search-results");
    createAutocomplete(".buyer-email", "#buyer-search-results")

    $("#clear").on( 'click', function () {
      $(".form-control").attr('value', '');
    });

  });