class DecryptTransactionJob < ApplicationJob
  require 'Decryptor'
  
  queue_as :decryptor

  rescue_from(ActiveRecord::RecordNotFound) do |exception|
    Delayed::Worker.logger.debug('The record of the transaction in the DB is missing')
  end

  rescue_from(ActiveJob::DeserializationError) do |exception|
    Delayed::Worker.logger.debug('The record of the transaction in the DB is missing')
  end

  def perform(transaction)
  	begin
  	  if !transaction.accepted and !transaction.rejected and transaction.recreatedPic.blank?
        file_path = Decryptor.decrypt(transaction.id, transaction.share1.current_path, transaction.share2.current_path)
        File.open(file_path) do |file|
  	      transaction.recreatedPic = file
	      end

        transaction.save!
        
        
        #dir_path = File.dirname(file_path)
        #FileUtils.rm_rf(dir_path)
        
        Delayed::Worker.logger.debug('Decryption done')
      end
    rescue WrongDimension => e 
      Delayed::Worker.logger.debug(e.message)
      transaction.rejected = true
      transaction.save
    rescue 
      Delayed::Worker.logger.debug(e.message)
      transaction.rejected = true
      transaction.save
    end  
  end
end
