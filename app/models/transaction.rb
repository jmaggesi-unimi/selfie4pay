class Transaction < ApplicationRecord
  belongs_to :seller, :class_name => 'User'
  belongs_to :buyer, :class_name => 'User'

  mount_base64_uploader :pictureBW, ImageUploader, file_name: 'bw_pic'
  mount_base64_uploader :pictureOriginal, ImageUploader, file_name: 'original_pic'
  mount_base64_uploader :share1, ImageUploader, file_name: 'share1'
  mount_base64_uploader :share2, ImageUploader, file_name: 'share2'
  mount_uploader :recreatedPic, ImageUploader, file_name: 'recreated_pic'

  scope :to_approve, -> { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected")
    .where.not('share1' => nil).where.not('share2' => nil).where.not('pictureBW' => nil).where.not('pictureOriginal' => nil)
    .where.not('recreatedPic' => nil).where('accepted' => false).where('rejected' => false) }

  scope :rejected, -> { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected")
    .where('accepted' => false).where('rejected' => true) }

  scope :accepted, -> { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected")
    .where('accepted' => true).where('rejected' => false) }

  scope :incomplete, -> { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected")
    .where('accepted' => false).where('rejected' => false).where("share1 IS NULL OR share2 IS NULL OR recreatedPic IS NULL") }

  scope :by_seller, -> (seller) { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected")
    .where('seller_id = ?', seller) }

  scope :by_buyer, -> (buyer) { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected")
    .where('buyer_id = ?', buyer) }

  scope :by_user, -> (user) { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected")
    .where('buyer_id = ? OR seller_id = ?', user, user) }

  scope :complete, -> { joins("LEFT OUTER JOIN users ON users.id = transactions.seller_id")
    .joins("LEFT OUTER JOIN users buyers_transactions ON buyers_transactions.id = transactions.buyer_id")
    .select("transactions.id, transactions.transactiontime, amount, seller_id, buyer_id, users.email AS seller_email, buyers_transactions.email AS buyer_email, accepted, rejected") }

  #scope :already_present?, -> (seller_id, buyer_id, timestamp) { where(:seller_id => seller_id, :buyer_id => buyer_id, :transactiontime => timestamp).first }

  def self.already_present?(seller_id, buyer_id, timestamp)
  	where(:seller_id => seller_id, :buyer_id => buyer_id, :transactiontime => timestamp).first
  end

  def is_decrypted?
    return true if not self.share1.blank? and not self.share2.blank? and not self.recreatedPic.blank? 
    false
  end

  def should_decrypt?
    return true if not self.share1.blank? and not self.share2.blank? and self.recreatedPic.blank?
    false
  end

  def sell_completed?
    return true if !transaction.pictureBW.blank? and !transaction.share1.blank? and !transaction.pictureOriginal.blank?
    false
  end

  def buy_completed?
    return true if !transaction.share2.blank?
    false
  end

  def is_completed?
    return true if !self.share1.blank? and !self.share2.blank? and !self.recreatedPic.blank? and !self.pictureOriginal.blank? and !self.pictureBW.blank?
    false 
  end

  def is_accepted?
    return true if self.accepted == true and self.rejected == false
    false
  end

  def have_amount?
    return true if self.amount > 0.0
    false
  end

  def is_rejected?
    return true if self.accepted == false and self.rejected == true
    false
  end

  def accept
    self.accepted = true
    self.rejected = false
    self.save
  end

  def reject
    self.accepted = false
    self.rejected = true
    self.save
  end

	def self.to_csv(options = {}, dump = false)
		columns = %w(id seller_email buyer_email transactiontime amount accepted rejected)

    #TODO: Dirty hack
		CSV.generate(options) do |csv|	
			csv << columns
			all.each do |transaction|
        elements = transaction.attributes.values_at(*columns)
        elements[1,2] = [transaction.seller.email, transaction.buyer.email] if dump
				csv << elements
			end
		end
  end

  private 

  # Whitelist the Transaction model attributes for sorting.
  #
  #
  def self.ransortable_attributes(auth_object = nil)
    column_names - ['pictureBW', 'share1', 'share2', 'pictureOriginal', 'recreatedPic']
  end

  # Whitelist the Transaction model attributes for search, except +password_digest+,
  #
  def self.ransackable_attributes(auth_object = nil)
    ransortable_attributes
  end


end
