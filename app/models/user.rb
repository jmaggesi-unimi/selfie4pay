class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_format_of :email, with: /\A([^\s]+)((?:[-a-z0-9]\.)[a-z]{2,})\z/i

  has_many :sales, :class_name => 'Transaction', :foreign_key => 'seller_id'
  has_many :purchases, :class_name => 'Transaction', :foreign_key => 'buyer_id'

  def is_admin?
  	self.is_admin
  end

  def self.find_by_email(email)
  	where(:email => email).first
  end

  def full_name
    return "#{name} #{surname}" if !name.nil? or !surname.nil? 
    return "Unnamed"
  end

  private 

  # Whitelist the Transaction model attributes for sorting.
  #
  #
  def self.ransortable_attributes(auth_object = nil)
    ['email', 'name', 'surname']
  end

  # Whitelist the Transaction model attributes for search, except +password_digest+,
  #
  def self.ransackable_attributes(auth_object = nil)
    ransortable_attributes
  end

  def self.search(term)
    where('LOWER(email) LIKE :term', term: "%#{term.downcase}%")
  end

end
