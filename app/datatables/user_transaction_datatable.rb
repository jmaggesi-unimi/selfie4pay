class UserTransactionDatatable < AjaxDatatablesRails::Base

  def view_columns
    @view_columns ||= {
      id: { source: 'Transaction.id', searchable: false, orderable: true, cond: :eq },
      seller: { source: 'seller_email', searchable: true, orderable: true, cond: filter_seller_email },
      buyer: { source: 'buyer_email', searchable: true, orderable: true , cond: filter_buyer_email },
      transactiontime: { source: 'Transaction.transactiontime', searchable: false, orderable: true },
      amount: { source: 'Transaction.amount', searchable: false, orderable: true }
    }
  end

  def data
    records.map do |transaction|
      {
        id: transaction.id,
        seller: transaction[:seller_email], #transaction.seller.try(:email),
        buyer: transaction[:buyer_email], #transaction.buyer.try(:email),
        amount: transaction.amount,
        transactiontime: transaction.transactiontime.to_s, #DateTime.strptime(transaction.transactiontime.to_s, "%Y-%m-%d %H:%M:%S"),
        DT_RowId: transaction.id
      }
    end
  end

  private

  def get_raw_records
    return Transaction.by_seller(options[:id]) if options[:status] == 'SELLER'
    return Transaction.by_buyer(options[:id]) if options[:status] == 'BUYER'
    return Transaction.by_user(options[:id]) if options[:status] == 'ALL'
  end


  def filter_seller_email
    ->(column) { ::Arel::Nodes::SqlLiteral.new('users.email').matches("#{ column.search.value }%") }
  end

  def filter_buyer_email
    ->(column) { ::Arel::Nodes::SqlLiteral.new('buyers_transactions.email').matches("#{ column.search.value }%") }
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
