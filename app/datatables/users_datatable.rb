class UsersDatatable < AjaxDatatablesRails::Base

  def view_columns
    @view_columns ||= {
      id: { source: 'User.id', searchable: false, orderable: true, cond: :eq },
      email: { source: 'User.email', searchable: true, orderable: true },
      name: { source: 'User.name', searchable: true, orderable: true },
      surname: { source: 'User.surname', searchable: true, orderable: true }
    }
  end

  def data
    records.map do |user|
      {
        id: user.id,
        email: user.email, #transaction.seller.try(:email),
        name: user.name, #transaction.buyer.try(:email),
        surname: user.surname,
        DT_RowId: user.id
      }
    end
  end

  private

  def get_raw_records
    User.all
  end


  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
