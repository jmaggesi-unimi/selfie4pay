class TransactionsController < ApplicationController
  require 'base64'
  require 'date'


  def index
    unless current_user.is_admin?
      redirect_to user_show_url(current_user)
    end
  end

  def edit
    if current_user.is_admin? and !params[:id].blank?
    	@transaction = Transaction.find(params[:id])
    else
    	redirect_to user_show_url(current_user)
    end
    
  end

  def update
  	amount = params[:transaction][:amount].to_d
  	@transaction = Transaction.find(params[:id])
  	
  	if !amount.blank? and amount > 0
  	  @transaction.amount = amount
  	
  	  if @transaction.save!
        redirect_to transaction_show_url(@transaction), notice: "Transaction Updated"
      else
    	  redirect_to transaction_edit_url(@transaction), alert: "Transaction cannot be updated"
      end
    else
    	redirect_to transaction_edit_url(@transaction), alert: "You cannot set the transaction amount to 0.0"
    end
  end

  def create
    uploaded_pics = params[:file]

    if !uploaded_pics.nil?
      uploaded_pics.each do |index,pic|
        regex = '^Pgs+\s+[a-zA-Z0-9]*_\d_\d+_[a-zA-Z0-9]+_PBS_[a-zA-Z]+'
        matcher = Regexp.new(regex,Regexp::IGNORECASE)

        original_filename = pic.original_filename
        filename = pic.original_filename.split('.')[0]

        if matcher.match(filename)
          info = filename.split('_')
          user_email = info[0].gsub('Pgs ','').concat("_" + info[1])
          timestamp = DateTime.strptime(info[2],"%d%m%Y%H%M%S")
          photo_type = info[3].downcase
          
          if info[5].downcase.include? 'buyer'
            transaction_type = 'buyer'
          else
            transaction_type = 'seller'
          end

          #debugger

          # Se l'utente esiste
          # TODO: Rimane il problema del mancato match in alcuni casi
          user = User.where(:email => user_email).or(User.where(:email => user_email+'@alphaplus.com')).first
          transaction = Transaction.where(:transactiontime => timestamp).first

          user.nil? ? user = create_fake_user(user_email) : user = user
          transaction.nil? ? transaction = Transaction.new : transaction = transaction

          case photo_type
          when 'gray'
            transaction.pictureOriginal = create_Base64_image(pic) if transaction.pictureOriginal.blank? and transaction_type == 'seller'
            transaction.pictureBW = create_Base64_image(pic) if transaction.pictureBW.blank? and transaction_type == 'buyer'
          when 'share1'
            transaction.share1 = create_Base64_image(pic) if transaction.share1.blank? and transaction_type == 'buyer'
          when 'share2'
            transaction.share2 = create_Base64_image(pic) if transaction.share2.blank? and transaction_type == 'seller'
          end
          
          if transaction.changed?
            transaction.seller = user if transaction_type == 'seller'
            transaction.buyer = user if transaction_type == 'buyer'
            transaction.transactiontime = timestamp
            transaction.save
            DecryptTransactionJob.perform_later(transaction) if transaction.should_decrypt?
          end
          # else
          #   # Non esiste l'utente e quindi lo creo
          #   user = create_fake_user(user_email)

          #end

        #else
          # Torna un errore il file non ha il nome corretto
        end
      end
    end

    render json: { message: 'You have successfully uploded your images.' }
    
  end

  def upload
    unless current_user.is_admin?
      redirect_to user_show_url(@current_user), alert: "Access Denied"
    end
  end

  # TODO: Protect only current_user and !current_user.blank?
  def transactions
  	respond_to do |format|
      format.html
      format.json { render json: TransactionDatatable.new(view_context, {:status => params[:status]}) }
    end
  end

  def show
  	if !params[:id].blank?
  	  begin
  	  	@transaction = Transaction.includes(:seller, :buyer).find(params[:id])
        if current_user == @transaction.seller or current_user == @transaction.buyer or current_user.is_admin?
          return @transaction
        else
          redirect_to root_url, alert: "You're not supposed to view the Transaction"
        end
  	  rescue ActiveRecord::RecordNotFound
  	  	redirect_to root_url, alert: "The Transaction doesn't exists" 
  	  	return
  	  end
  	else
  	  redirect_to root_url, alert: "The Url is wrong"  
    end
  end

  #TODO: Clumsy code -- Please rewrite
  def accept
  	if current_user && current_user.is_admin?
  	  if !params[:id].blank?
  	    t = Transaction.find(params[:id])
  	    if t.is_completed? and t.have_amount?
  	    	if t.accept
  		  		redirect_to transaction_show_url(t), notice: "Transaction Accepted"
  	    	else
  		  		redirect_to transaction_show_url(t), alert: "Transaction cannot be accepted"
  				end
  			else
  				redirect_to transaction_show_url(t), alert: "Check if transaction is completed or if the amount is bigger than 0.0"
  	    end
  	  end
  	else
  		redirect_to root_url, alert: "Log in as an admin to perform action"
  	end
  end

  #TODO: Clumsy code -- Please rewrite
  def reject
  	if current_user && current_user.is_admin?
  		if !params[:id].blank?
  			t = Transaction.find(params[:id])
  			if t.is_completed? and t.have_amount?
  				if t.reject
  					redirect_to transaction_show_url(t), notice: "Transaction Rejected"
  				else
  					redirect_to transaction_show_url(t), alert: "Transaction cannot be accepted"
  				end
  			else
  				redirect_to transaction_show_url(t), alert: "Check if transaction is completed or if the amount is bigger than 0.0"
  			end
  		end
  	else
  		redirect_to root_url, alert: "Log in as an admin to perform action"
  	end
  end

  def all
    if current_user.is_admin?
    	@transactions = Transaction.complete
			respond_to do |format|
				format.csv { send_data @transactions.to_csv }
				format.xls # { send_data @products.to_csv(col_sep: "\t") }
			end
    end
  end

  def toapprove
    if current_user.is_admin?
    	@transactions = Transaction.to_approve
			respond_to do |format|
				format.csv { send_data @transactions.to_csv }
				format.xls # { send_data @products.to_csv(col_sep: "\t") }
			end
    end
  end

  def rejected
    if current_user.is_admin?
    	@transactions = Transaction.rejected
			respond_to do |format|
				format.csv { send_data @transactions.to_csv }
				format.xls # { send_data @products.to_csv(col_sep: "\t") }
			end
    end
  end

  def accepted
    if current_user.is_admin?
    	@transactions = Transaction.accepted
			respond_to do |format|
				format.csv { send_data @transactions.to_csv }
				format.xls # { send_data @products.to_csv(col_sep: "\t") }
			end
    end
  end

  def incomplete
    if current_user.is_admin?
    	@transactions = Transaction.incomplete
			respond_to do |format|
				format.csv { send_data @transactions.to_csv }
				format.xls # { send_data @products.to_csv(col_sep: "\t") }
			end
    end
  end

  #TODO: Add pagination
  def search
    if !params[:q].blank?
      clear_boolean(params[:q], :accepted_eq)
      clear_boolean(params[:q], :rejected_eq)
    end

    if !ransack_parameter[:q].blank?
      @transact_less_than = ransack_parameter[:q][:transactiontime_lteq] if !ransack_parameter[:q][:transactiontime_lteq].blank?
      @transact_great_than = ransack_parameter[:q][:transactiontime_gteq] if !ransack_parameter[:q][:transactiontime_gteq].blank?
    end

    @search = Transaction.ransack(ransack_parameter[:q])
    @transactions = @search.result(distinct: true).includes(:seller, :buyer)
    
    respond_to do |format|
        format.html
        format.csv { send_data @transactions.to_csv({}, true), :type => 'text/csv; header=present', :filename => 'dump.csv' }
        format.xls # { send_data @products.to_csv(col_sep: "\t") }
    end
  end

  private

  #TODO: Move to model method
  def create_Base64_image(picture)
    base64_image = Base64.strict_encode64(picture.read)
    base64_image.strip!
    image = 'data:'+ picture.content_type + ';base64,' + base64_image
    return image
  end

  def create_fake_user(user_email)
    user = User.new
    user.email = user_email + '@alphaplus.com'
    user.password = "DEMOPASSWORD"
    user.name = user_email
    user.save!
    user
  end

  def clear_boolean(q, condition)
    q.delete(condition) if q[condition] == "0"
  end

  def ransack_parameter
    params.permit(:q => [:seller_email_eq, :buyer_email_eq, :accepted_eq, :rejected_eq, :transactiontime_gteq, :transactiontime_lteq])
  end

end
