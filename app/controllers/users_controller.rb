class UsersController < ApplicationController

	def sales
  	respond_to do |format|
      format.html
      format.json { render json: UserTransactionDatatable.new(view_context, {:status => params[:status], :id => params[:id]}) }
    end
  end

  def all_transactions
  	respond_to do |format|
      format.html
      format.json { render json: UserTransactionDatatable.new(view_context, {:status => params[:status], :id => params[:id]}) }
    end
  end

  def purchases
  	respond_to do |format|
      format.html
      format.json { render json: UserTransactionDatatable.new(view_context, {:status => params[:status], :id => params[:id]}) }
    end
  end

  def show
  	if !params[:id].nil?
	  	@user = User.find(params[:id])
	  	if current_user.is_admin? or current_user == @user
	  	  render :show
	  	else
	  	  redirect_to user_show_url(current_user), alert: 'Unauthorized to look at the page of the user'
	  	end
	  else
	  	redirect_to user_show_url(current_user)
	  end
  end

  def index
  	if current_user
  	  if current_user.is_admin? 
  	  	render 'index'
  	  else 
  	  	redirect_to user_show_url(current_user), alert: 'Not Allowed to look at the page'	
  	  end
  	else
  	  redirect_to new_user_session_url 
  	end
  end

  
  def by_seller
  	if !params[:id].nil?
  		@user = User.find(params[:id])
	  	if current_user.is_admin? or current_user == @user
	  	  @transactions = Transaction.by_seller(params[:id])
	  	  respond_to do |format|
					format.csv { send_data @transactions.to_csv }
					format.xls # { send_data @products.to_csv(col_sep: "\t") }
				end
	  	else
	  	  redirect_to user_show_url(current_user), alert: 'Cannot download the data'
	  	end
	  else
	  	redirect_to user_show_url(current_user)
	  end
  end

  def by_buyer
  	if !params[:id].nil?
  		@user = User.find(params[:id])
	  	if current_user.is_admin? or current_user == @user
	  	  @transactions = Transaction.by_buyer(params[:id])
	  	  respond_to do |format|
					format.csv { send_data @transactions.to_csv }
					format.xls # { send_data @products.to_csv(col_sep: "\t") }
				end
	  	else
	  	  redirect_to user_show_url(current_user), alert: 'Cannot download the data'
	  	end
	  else
	  	redirect_to user_show_url(current_user)
	  end
  end

  def all
  	if !params[:id].nil?
  		@user = User.find(params[:id])
	  	if current_user.is_admin? or current_user == @user
	  	  @transactions = Transaction.by_user(params[:id])
	  	  respond_to do |format|
					format.csv { send_data @transactions.to_csv }
					format.xls # { send_data @products.to_csv(col_sep: "\t") }
				end
	  	else
	  	  redirect_to user_show_url(current_user), alert: 'Cannot download the data'
	  	end
	  else
	  	redirect_to user_show_url(current_user)
	  end
  end

  def all_users
  	respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context) }
    end
  end

  def search
    respond_to do |format|
      format.html
      format.json { @users = User.search(params[:term]) }
    end
  end


end
