class ImagesController < ApplicationController

	def download

		method_transaction_name = params[:mount]
		# path = transaction.public_send(method_name) if transaction.respond_to? method_name

  	# original_path = "#{path.path}"
  	
  	#'#{Rails.root}/uploads/transaction/#{}'+method_transaction_name+'/'+params[:id].to_s+'/'+params[:basename] + '.' +params[:extension]
		#original_path = '#{Rails.root}/uploads/transaction/#{method_transaction_name}/#{params[:id]}/#{params[:basename]}.{params[:extension]}'
		filename = params[:basename] + '.' + params[:extension]
		path = Rails.root.join('uploads', 'transaction', method_transaction_name, params[:id], filename)
		send_file path, :x_sendfile=>true
	end

end