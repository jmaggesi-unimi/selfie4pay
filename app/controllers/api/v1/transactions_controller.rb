class Api::V1::TransactionsController < Api::V1::BaseController

  def sell
    #TODO: Check if picture exists 
    #TODO: Se la transazione esiste già allora dobbiamo fare l'update o impedirlo 
    seller = User.find_by_email(seller_params[:seller])
    buyer = User.find_by_email(seller_params[:buyer])
    status = 200
    if !seller.nil? and !buyer.nil?
      transaction = Transaction.already_present?(seller, buyer, seller_params[:transactiontime])
      if !transaction.nil? and !transaction.sell_completed?
        status = 200
        transaction.pictureBW = seller_params[:pictureBW]
        transaction.share1 = seller_params[:share1]
        transaction.pictureOriginal = seller_params[:pictureOriginal]
      else
        status = 201
        transaction = Transaction.new()
        transaction.seller = seller
        transaction.buyer = buyer
        transaction.transactiontime = seller_params[:transactiontime]
        transaction.pictureBW = seller_params[:pictureBW]
        transaction.share1 = seller_params[:share1]
        transaction.pictureOriginal = seller_params[:pictureOriginal]
      end

      reply(transaction, status)

    else
      render json: { errors: 'Transaction creation failed' }, status: 422
    end
  end

  def buy
    seller = User.find_by_email(buyer_params[:seller])
    buyer = User.find_by_email(buyer_params[:buyer])
    status = 200
    if !seller.nil? and !buyer.nil?
      transaction = Transaction.already_present?(seller, buyer, buyer_params[:transactiontime])
      if !transaction.nil? and !transaction.buy_completed?
        status = 200
        transaction.share2 = buyer_params[:share2] if !buyer_params[:share2].blank?
      else
        status = 201
        transaction = Transaction.new()
        transaction.seller = seller
        transaction.buyer = buyer
        transaction.transactiontime = buyer_params[:transactiontime]
        transaction.share2 = buyer_params[:share2] if !buyer_params[:share2].blank?
      end

      reply(transaction, status)

    else
      render json: { errors: 'Transaction creation failed' }, status: 422
    end
  end

private

  def seller_params
    params.permit(:seller, :buyer, :transactiontime, :pictureBW, :share1, :pictureOriginal, :amount, :access_token)
  end

  def buyer_params
    params.permit(:seller, :buyer, :transactiontime, :share2, :amount, :access_token)
  end

  def reply(transaction, status)
    if transaction.save
      DecryptTransactionJob.perform_later(transaction) if transaction.should_decrypt?
      render json: { transaction_id: transaction.id }, status: status
    else
      render json: { errors: transaction.errors }, status: 422
    end
  end

end