class Api::V1::SignupController < Api::V1::BaseController
  skip_before_action :doorkeeper_authorize!
  skip_before_action :authenticate_user!

  def index
    user = User.new(signup_params)

    if user.save
      render json: { user_id: user.id, name: user.name, surname: user.surname, email: user.email }, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end

private

  def signup_params
    params.permit(:email, :password, :name, :surname)
  end
end