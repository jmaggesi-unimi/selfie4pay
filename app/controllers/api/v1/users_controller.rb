class Api::V1::UsersController < Api::V1::BaseController

  def index
    users = User.all
    
    #render json: { users: users.to_json }, each_serializer: UserSerializer
    render json: { users: users.to_json }
  end

  def update
    user = User.find(params[:id])
    if user.update(update_params)
      #render json: user, serializer: UserSerializer, status: 200
      render json: user.to_json, status: 200
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def prova
    prova = params[:prova]
    logger.debug prova
    prova2 = params[:prova2]
    logger.debug prova2

    render json: { users: current_user.to_json }
  end

private

  def update_params
    params.permit(:email, :password)
  end
end