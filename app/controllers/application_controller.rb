class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  protected

  # Add my attributes added to the devise User class
  def configure_permitted_parameters
  	added_attrs = [:surname, :name, :email, :password, :password_confirmation, :remember_me] 
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def record_not_found
  	redirect_to :root, alert: 'The page searched is inexistent'
  end

end
