module ApplicationHelper
  def active_class(link_path)
    current_page?(link_path) ? "active" : ""
  end

  def status(transaction)
  	return '<span class="tag tag-success badge pull-xs-right">ACCEPTED</span>'.html_safe if transaction.is_completed? and transaction.is_accepted?
  	return '<span class="tag tag-danger badge pull-xs-right">REJECTED</span>'.html_safe if transaction.is_completed? and transaction.is_rejected?
  	'<span class="tag tag-warning badge pull-xs-right">PENDING</span>'.html_safe
  end

  def transaction_image(transaction_id, method, image, thumb)
    return '/uploads/transaction/' + method + '/example/'+transaction_id.to_s+'/'+File.basename(image.thumb.url) if !image.blank? and thumb
    return '/uploads/transaction/' + method + '/example/'+transaction_id.to_s+'/'+File.basename(image.medium.url) if !image.blank? and !thumb
    return '/not-available.png'
  end

  def transaction_time(transaction_time)
  	transaction_time.blank? ? '' : transaction_time.strftime("%d %b %y %k:%M:%S")
  end
end
