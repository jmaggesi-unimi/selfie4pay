class Decryptor
  require 'rmagick'
  require 'fileutils'
    
  # Casi da prendere in esame:
  # 1 Le dimensioni tra i file sono diverse --> Porta alla Reject diretta della transazione
  # 2 Una delle 2 immagini non è valida/nil --> Porta alla Reject diretta della transazione
  # 3 Magick::ImageMagickError --> Porta alla Reject diretta della transazione ?????????
  def self.decrypt(transaction_id, share1_path, share2_path)
	  share1 = Magick::Image.read(share1_path).first
	  share2 = Magick::Image.read(share2_path).first

	  #start = Time.now

	  width = share1.columns
	  height = share1.rows
	
	  raise WrongDimension, 'The dimensions of the pics should be equal' if width != share2.columns and height != share2.rows

	  final_image = Magick::Image.new(width/2, height)
	
	  (0..width).step(2) do |col|
	    break if(col/2 == width/2)
	      
	    #TODO: Provare a prendere la colonna intera di pixel e fare il ciclo su quelli
	    (0..height).each do |row|
	      if(share1.pixel_color(col,row) == share2.pixel_color(col,row))
	        final_image.pixel_color(col/2,row,'white')
	      else
	        final_image.pixel_color(col/2,row,'black')
	      end
	    end 
	  end
	
    share1.destroy!
    share2.destroy!

	  self.save_decrypted_image(transaction_id, final_image)
	  #Time.now - start
  end
  
  private
  
  def self.save_decrypted_image(transaction_id, final_image)
    dir_name = Rails.root.join('tmp').to_s + '/Decryptor/' + transaction_id.to_s
    key = transaction_id.to_s + '-' + Time.now.to_i.to_s + '.jpg'

    FileUtils.mkdir_p(dir_name) if !File.directory?(dir_name)
    
    file_name = dir_name + '/' + key

    final_image.write(file_name)
    final_image.destroy!
    file_name                     
  end
	
end

class WrongDimension < StandardError
  attr_reader :message

  def initialize(message)
   super
   @message = message
  end
end

