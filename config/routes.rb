Rails.application.routes.draw do

  use_doorkeeper
  devise_for :users

  namespace :api do
  	namespace :v1 do
  		resources :users, only: [:index, :update]
  		post '/users/prova' => 'users#prova'
  		post '/transactions/sell' => 'transactions#sell'
  		post '/transactions/buy' => 'transactions#buy'
    	post :signup, to: 'signup#index'
  	end
  end

  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  get 'transactions/index' => 'transactions#index'
  get 'transactions/transactions' => 'transactions#transactions'
  get 'transactions' => 'transactions#index'

  get 'transactions/search' => 'transactions#search', as: :transactions_search #used by the ransack page

  get 'users/index' => 'users#index', as: :users_index
  get 'users/all' => 'users#all_users', as: :users_all

  get 'user/all_transactions' => 'users#all_transactions', as: :user_transactions
  get 'user/purchases' => 'users#purchases', as: :user_purchases
  get 'user/sales' => 'users#sales', as: :user_sales
  get 'user/search' => 'users#search', as: :user_search

  get 'user/download_sells' => 'users#by_seller', as: :user_sells
  get 'user/download_purchases' => 'users#by_buyer', as: :user_buys
  get 'user/download_all' => 'users#all', as: :user_alltransactions

  get 'user/:id' => 'users#show', as: :user_show
  
  get 'transactions/accept/:id' => 'transactions#accept', as: :transaction_accept
  get 'transactions/reject/:id' => 'transactions#reject', as: :transaction_reject
  post 'transactions/create' => 'transactions#create', as: :transaction_create
  get 'transactions/upload' => 'transactions#upload', as: :transaction_upload

  get 'transactions/all' => 'transactions#all', as: :transactions_all
  get 'transactions/toapprove' => 'transactions#toapprove', as: :transactions_toapprove
  get 'transactions/rejected' => 'transactions#rejected', as: :transactions_rejected
  get 'transactions/accepted' => 'transactions#accepted', as: :transactions_accepted
  get 'transactions/incomplete' => 'transactions#incomplete', as: :transactions_incomplete


  get 'transactions/:id' => 'transactions#show', as: :transaction_show
  get 'transactions/:id/edit' => 'transactions#edit', as: :transaction_edit
  match 'transactions/:id/update' => 'transactions#update', as: :transaction_update, :via => [:post, :patch]

  get "/uploads/transaction/:mount/example/:id/:basename.:extension", :controller => "images", :action => "download"

  authenticated :user do
    root :to => "transactions#index"
  end

  unauthenticated :user do
    devise_scope :user do
      get "/" => "devise/sessions#new"
    end
  end

  
end
