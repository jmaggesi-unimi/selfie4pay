class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.references :seller, index: true
      t.references :buyer, index: true
      t.datetime :transactiontime
      t.decimal :amount, :precision => 8, :scale => 2
      t.string :pictureBW
      t.string :pictureOriginal
      t.string :share1
      t.string :share2
      t.string :recreatedPic

      t.timestamps
    end

    add_index :transactions, :transactiontime
    add_foreign_key :transactions, :users, column: :seller_id
    add_foreign_key :transactions, :users, column: :buyer_id
  end
end
