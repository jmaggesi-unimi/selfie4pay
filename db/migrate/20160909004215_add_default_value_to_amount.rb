class AddDefaultValueToAmount < ActiveRecord::Migration[5.0]
  def change
  	change_column_default :transactions, :amount, 0.0
  end
end
