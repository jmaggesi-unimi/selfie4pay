class AddAcceptedandRejectedToTransaction < ActiveRecord::Migration[5.0]
  def change
    add_column :transactions, :accepted, :boolean, :default => false
    add_column :transactions, :rejected, :boolean, :default => false
  end
end
